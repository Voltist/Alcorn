# Alcorn

A virtual game of pong that is played by physically moving. Inspired by the (never built) 'Trans-American Pong' from [Matt and Tom - Rejects from Tom's Ideas Board 2: The Modern Years](https://www.youtube.com/watch?v=-MyrZF5QGVo).

## Getting started
To start the Alcorn server, run server.py with the coordinates of the lower left and upper right corners of the desired play area, as well as the initial speed (recommended is 10):
```
python server.py <lllatitude> <lllongitude> <urlatitude> <urlongitude> <km/h_init>
```
*Note: Commas in the coordinates will be ignored, so you can copy and paste from google maps without fear :)*

## Authors
* **Liam Scaife** - *Initial work*
* **Linus Molteno** - *A second opinion*

## License
This project is licensed under the GNU General Public License - see the [LICENSE](LICENSE.md) file for details.
