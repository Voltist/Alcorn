from flask import Flask, request
from flask_restful import Resource, Api
from json import dumps
from flask_cors import CORS
import time
import random
import sys
from math import sin, cos, sqrt, atan2, radians

def dis(la1, lo1, la2, lo2):
    # approximate radius of earth in km
    R = 6373.0

    lat1 = radians(la1)
    lon1 = radians(lo1)
    lat2 = radians(la2)
    lon2 = radians(lo2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    return R * c

app = Flask(__name__)
CORS(app)
api = Api(app)

args_temp = sys.argv
del args_temp[0]
args = [0, 0, 0, 0]
args[0] = float(args_temp[2].replace(",", ""))
args[1] = float(args_temp[0].replace(",", ""))
args[2] = float(args_temp[3].replace(",", ""))
args[3] = float(args_temp[1].replace(",", ""))

pos = [(args[0] + args[1]) / 2, (args[2] + args[3]) / 2]
d = dis(args[0], args[2], args[1], args[3]) / float(args_temp[4]) / 60.0 / 60.0
ropt = [[d, d/2], [-d, -d/2]]
rate = random.choice(ropt)

num = 0
class players(Resource):
    def post(self):
        global num
        global args
        global pos
        print num
        if num < 2:
            num = num + 1
            return {"player": num, "args": args, "center": pos}
        else:
            return {"player": "Game full"}
    def get(self):
        return {"num": num}

score = [0, 0]

paddles = {'1': [0, 0], '2': [0, 0]}

going = False
class loc(Resource):
    def post(self):
        global score
        global start
        global paddles
        global pos
        global going
        global rate

        if request.form.get("player") == '2' and going == False:
            going = True
            start = time.time()
        elif request.form.get("player") == "1" and going == False:
            return {"waiting": "yes"}
        l = time.time() - start

        padloc = map(float, request.form.get("padloc").split(","))
        player = request.form.get("player")
        paddles[player] = padloc

        add = False
        tmp = args[1] + ((args[0] - args[1]) * 0.07)
        if pos[0] + (rate[0] * l) < tmp and padloc[1] > pos[1] + (rate[1] * l) > padloc[0]:
            add = True
            print "paddle"
            pos[0] = tmp
            pos[1] = pos[1] + (rate[1] * l)
            start = time.time()

            r = float(random.choice(range(3))) / 10
            if random.choice([True, False]):
                rate[0] = 0 - rate[0] * (1 + r)
                rate[1] = rate[1] * (1 - r)
            else:
                rate[0] = 0 - rate[0] * (1 - r)
                rate[1] = rate[1] * (1 + r)
            l = time.time() - start

        tmp = args[0] - ((args[0] - args[1]) * 0.07)
        if pos[0] + (rate[0] * l) > tmp and padloc[1] > pos[1] + (rate[1] * l) > padloc[0]:
            add = True
            print "paddle"
            pos[0] = tmp
            pos[1] = pos[1] + (rate[1] * l)
            start = time.time()

            r = float(random.choice(range(3))) / 10
            if random.choice([True, False]):
                rate[0] = 0 - rate[0] * (1 + r)
                rate[1] = rate[1] * (1 - r)
            else:
                rate[0] = 0 - rate[0] * (1 - r)
                rate[1] = rate[1] * (1 + r)
            l = time.time() - start

        if pos[0] + (rate[0] * l) > args[0]:
            add = True
            score[1] += 1
            rate = random.choice(ropt)
            pos = [(args[0] + args[1]) / 2, (args[2] + args[3]) / 2]
            start = time.time()
            l = time.time() - start
        if pos[0] + (rate[0] * l) < args[1]:
            add = True
            score[0] += 1
            rate = random.choice(ropt)
            pos = [(args[0] + args[1]) / 2, (args[2] + args[3]) / 2]
            start = time.time()
            l = time.time() - start

        if pos[1] + (rate[1] * l) > args[2]:
            print 'jump3'
            pos[1] = args[2]
            pos[0] = pos[0] + (rate[0] * l)
            start = time.time()
            rate[1] = 0 - rate[1]
            l = time.time() - start
        if pos[1] + (rate[1] * l) < args[3]:
            print "jump4"
            pos[1] = args[3]
            pos[0] = pos[0] + (rate[0] * l)
            start = time.time()
            rate[1] = 0 - rate[1]
            l = time.time() - start

        if add:
            rate[0] = rate[0] * 1.1
            rate[1] = rate[1] * 1.1

        if player == '1':
            other = paddles['2']
        else:
            other = paddles['1']
        print paddles['1']

        return {"waiting": "no", "loc": {"lat": pos[0] + (rate[0] * l), "lng": pos[1] + (rate[1] * l)}, "score": "-".join(map(str, score)), "other": other}

class reset(Resource):
    def post(self):
        global num
        global score
        global start
        global paddles
        global pos
        global going
        global rate
        global args
        score = [0, 0]
        start = time.time()
        rate = random.choice(ropt)
        return {"success": True}

api.add_resource(reset, '/api/reset')
api.add_resource(players, '/api/players')
api.add_resource(loc, '/api/loc')
if __name__ == '__main__':
     app.run(port='8000')
