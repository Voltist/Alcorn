var map;
var circle;
var paddles;
var locs;
var lower_bar;
var upper_bar;

$(document).ready(function() {
  $.ajax({
    type: "POST",
    crossDomain: true,
    url: "/api/players",
  }).done(function(data) {
    console.log(data);
    if (data["player"] != "Game full") {
      sessionStorage.setItem("player", data["player"]);

      locs = data["args"];
      map = new google.maps.Map(document.getElementById('map'), {
        center: {
          lat: data["center"][0],
          lng: data["center"][1]
        },
        zoom: 17,
        disableDefaultUI: true,
	mapTypeId: 'satellite'
      });

      var rectangle = new google.maps.Rectangle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.05,
        map: map,
        bounds: {
          south: locs[1],
          north: locs[0],
          west: locs[3],
          east: locs[2]
        }
      });

      circle = new google.maps.Circle({
        strokeColor: 'black',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: 'black',
        fillOpacity: 0.90,
        map: map,
        center: {
          lat: data["center"][0],
          lng: data["center"][1]
        },
        radius: 2.5
      });

      lower_bar = locs[1] + ((locs[0] - locs[1]) * 0.07);
      var lower_line = new google.maps.Rectangle({
        strokeColor: 'grey',
        strokeOpacity: 0.8,
        strokeWeight: 0.5,
        fillColor: 'grey',
        fillOpacity: 0.0,
        map: map,
        bounds: {
          south: lower_bar,
          north: lower_bar,
          west: locs[3],
          east: locs[2]
        }
      });

      upper_bar = locs[0] - ((locs[0] - locs[1]) * 0.07);
      var upper_line = new google.maps.Rectangle({
        strokeColor: 'grey',
        strokeOpacity: 0.8,
        strokeWeight: 0.5,
        fillColor: 'grey',
        fillOpacity: 0.0,
        map: map,
        bounds: {
          south: upper_bar,
          north: upper_bar,
          west: locs[3],
          east: locs[2]
        }
      });

      paddles = {
        '2': new google.maps.Rectangle({
          strokeColor: 'black',
          strokeOpacity: 0.8,
          strokeWeight: 1,
          fillColor: 'grey',
          fillOpacity: 1,
          map: map,
          bounds: {
            south: lower_bar - ((locs[0] - locs[1]) * 0.01),
            north: lower_bar + ((locs[0] - locs[1]) * 0.01),
            west: data["center"][1] - ((locs[2] - locs[3]) * 0.1),
            east: data["center"][1] + ((locs[2] - locs[3]) * 0.1)
          }
        }),

        '1': new google.maps.Rectangle({
          strokeColor: 'black',
          strokeOpacity: 0.8,
          strokeWeight: 1,
          fillColor: 'grey',
          fillOpacity: 1,
          map: map,
          bounds: {
            south: upper_bar - ((locs[0] - locs[1]) * 0.01),
            north: upper_bar + ((locs[0] - locs[1]) * 0.01),
            west: data["center"][1] - ((locs[2] - locs[3]) * 0.1),
            east: data["center"][1] + ((locs[2] - locs[3]) * 0.1)
          }
        })
      };

      $("#welcome").html("Welcome player " + data["player"]);
      if (data["player"] == 1) {
        $("#loading").html("Awaiting player 2 to confirm game start...");
        var t = setInterval(move, 100);
      } else {
        $("#loading").replaceWith("<button onclick='start()' style='background-color: red'>Press here to start game!</button>");
      }
    } else {
      alert("Game full!");
    }

    window.onerror = function(msg, url, linenumber) {
      alert('Error message: ' + msg + '\nURL: ' + url + '\nLine Number: ' + linenumber);
      return true;
    };
  });
});

function reset() {
  $.ajax({
    type: "POST",
    crossDomain: true,
    url: "/api/reset",
  }).done(function(data) {
    console.log(data);
  });
}

function start() {
  var t = setInterval(move, 100);
}

function move() {
  $.ajax({
    type: "POST",
    crossDomain: true,
    url: "/api/loc",
    data: {
      padloc: sessionStorage.getItem("latlon"),
      player: sessionStorage.getItem("player")
    }
  }).done(function(data) {
    console.log(data);
    if (data["waiting"] == "no") {
      $("#overlay").hide();
      $("#score").html(data["score"]);
      circle.setMap(null);
      circle = new google.maps.Circle({
        strokeColor: 'black',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: 'black',
        fillOpacity: 0.90,
        map: map,
        center: data["loc"],
        radius: 2.5
      });

      if (sessionStorage.getItem("player") == '1') {
        paddles['2'].setMap(null);
        paddles['2'] = new google.maps.Rectangle({
          strokeColor: 'black',
          strokeOpacity: 0.8,
          strokeWeight: 1,
          fillColor: 'grey',
          fillOpacity: 1,
          map: map,
          bounds: {
            south: lower_bar - ((locs[0] - locs[1]) * 0.01),
            north: lower_bar + ((locs[0] - locs[1]) * 0.01),
            west: data["other"][0],
            east: data["other"][1]
          }
        });
      } else {
        paddles['1'].setMap(null);
        paddles['1'] = new google.maps.Rectangle({
          strokeColor: 'black',
          strokeOpacity: 0.8,
          strokeWeight: 1,
          fillColor: 'grey',
          fillOpacity: 1,
          map: map,
          bounds: {
            south: upper_bar - ((locs[0] - locs[1]) * 0.01),
            north: upper_bar + ((locs[0] - locs[1]) * 0.01),
            west: data["other"][0],
            east: data["other"][1]
          }
        });
      }
    }
  });
}

var watchID = navigator.geolocation.watchPosition(uppos, error, {
  enableHighAccuracy: true,
  maximumAge: 0
});
sessionStorage.setItem("latlon", [170.5269, 170.5274]);

function error(err) {
  alert('ERROR(' + err.code + '): ' + err.message);
}

function uppos(position) {
  var player = sessionStorage.getItem("player");
  if (player == '1') {
    paddles[player].setMap(null);
    var ol = [0, 0];
    ol[1] = position.coords.longitude + ((locs[2] - locs[3]) * 0.1);
    ol[0] = position.coords.longitude - ((locs[2] - locs[3]) * 0.1);
    console.log(ol);
    sessionStorage.setItem("latlon", ol);
    paddles[player] = new google.maps.Rectangle({
      strokeColor: 'black',
      strokeOpacity: 0.8,
      strokeWeight: 1,
      fillColor: 'grey',
      fillOpacity: 1,
      map: map,
      bounds: {
        south: upper_bar - ((locs[0] - locs[1]) * 0.01),
        north: upper_bar + ((locs[0] - locs[1]) * 0.01),
        west: ol[0],
        east: ol[1]
      }
    });
  } else {
    paddles[player].setMap(null);
    var ol = [0, 0];
    ol[1] = position.coords.longitude + ((locs[2] - locs[3]) * 0.1);
    ol[0] = position.coords.longitude - ((locs[2] - locs[3]) * 0.1);
    console.log(ol);
    sessionStorage.setItem("latlon", ol);
    paddles[player] = new google.maps.Rectangle({
      strokeColor: 'black',
      strokeOpacity: 0.8,
      strokeWeight: 1,
      fillColor: 'grey',
      fillOpacity: 1,
      map: map,
      bounds: {
        south: lower_bar - ((locs[0] - locs[1]) * 0.01),
        north: lower_bar + ((locs[0] - locs[1]) * 0.01),
        west: ol[0],
        east: ol[1]
      }
    });
  }

}
